# Testing Redis/PubSub/Golang/Websockets 

## Building

```
go build .
sudo ./channels # Has to run with privilages to bind to port :80
```

## Test Client
There is an included test client, which will create 30 fake users and subscribes them to either the
channel "foo" or "bar", any messages received by the client will be written to
`results/$WORKERID_worker.log`

### Building

```
cd testclient
mkdir 
go build . 
./testclient 
```

## Usage 

Creates a websocket on localhost:80, can connect to this with
`websocat ws://localhost/v1/socket`

* Sending the message "user" down the socket will name your user Dave Mustaine
* Sending the message "channel" will attempt to join the pubsub channel "foo"
* Sending the message "debug" will print a list of the available channels and subscribers

### Debugging

There is a debugging page, which can be accessed at the url: `http://localhost/v1/debugging`

![debugging webpage screenshot](img/debugging_page.png)
