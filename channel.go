package main

import (
	"context"
	"sync"

	// "github.com/go-redis/redis/extra/redisotel/v8"
	"github.com/google/uuid"
	// "go.opentelemetry.io/otel/attribute"
)

type HubChannel struct {
	ID          string
	Subscribers struct {
		mu   sync.Mutex
		Data map[uuid.UUID]*user
	}
}

type channelsMap struct {
	mu   sync.Mutex
	data map[string]*HubChannel
	wg   *sync.WaitGroup
}

var ChannelMap = channelsMap{
	data: make(map[string]*HubChannel),
}

var channelsMapMux sync.RWMutex
var subscriberMux sync.RWMutex
var creationWaitGroup sync.WaitGroup

func (c *HubChannel) addToChannelMap() {
	mu := &ChannelMap.mu
	mu.Lock()
	defer mu.Unlock()

	ChannelMap.data[c.ID] = c
}

func (c *HubChannel) deleteFromChannelMap() {
	mu := &ChannelMap.mu
	mu.Lock()
	defer mu.Unlock()

	delete(ChannelMap.data, c.ID)
}

func (c *HubChannel) init() {
	logger(LogFields{"hubchannel", "init", c.ID, ""}).Info("channel init")
	joinRedisChannel(c.ID)
	logger(LogFields{"hubchannel", "init", c.ID, ""}).Debug("creating channel subscribers map")
	c.Subscribers.Data = make(map[uuid.UUID]*user, 20)
}

func (c *HubChannel) remove() {
	logger(LogFields{"hubchannel", "remove", c.ID, ""}).Warn("Closing PubSub connection")
	leaveRedisChannel(c.ID)
	c.deleteFromChannelMap()
	logger(LogFields{"hubchannel", "remove", c.ID, ""}).Warn("Removing channel from map")
}

func addSubscriberToChannel(userPointer *user, channelid string) {
	logger(LogFields{"hubchannel", "addSubscriberToChannel", channelid, userPointer.Name}).Debug("Adding user to channel")
	channel := ChannelMap.data[channelid]
	channel.Subscribers.mu.Lock()
	defer channel.Subscribers.mu.Unlock()
	channel.Subscribers.Data[userPointer.ID] = userPointer
}

func (c *HubChannel) removeSubscriber(userid uuid.UUID) {
	logger(LogFields{"hubchannel", "removeSubscriber", c.ID, ""}).Debug("Removing user: ", userid)
	c.Subscribers.mu.Lock()
	defer c.Subscribers.mu.Unlock()

	delete(c.Subscribers.Data, userid)
}

func (c *HubChannel) handleIncMessage(msg []byte) {
	logger(LogFields{"hubchannel", "handHubMessages", c.ID, ""}).Debug("Incomming message from pubsub:", string(msg))
	increaseTotalIncMessageCount()
	for _, subscriber := range c.Subscribers.Data {
		increaseTotalSentMessageCount()
		subscriber.msgQueue <- string(msg)
	}
}

func createChannel(channelid string, ctx context.Context) HubChannel {
	// _, span := tracer.Start(ctx, "findorcreatechannel")
	// defer span.End()

	creationWaitGroup.Add(1)
	logger(LogFields{"hubchannel", "createChannel", channelid, ""}).Warn("creating channel")
	c := HubChannel{
		ID: channelid,
	}
	c.init()
	c.addToChannelMap()
	creationWaitGroup.Done()
	return c
}

func findOrCreateChannel(channelid string, ctx context.Context) HubChannel {
	// _, span := tracer.Start(ctx, "findorcreatechannel")
	// defer span.End()

	creationWaitGroup.Wait()
	logger(LogFields{"hubchannel", "findOrCreateChannel", channelid, ""}).Debug("finding or creating channel")
	channel, exists := ChannelMap.data[channelid]
	if exists {
		return *channel
	}
	newchannel := createChannel(channelid, ctx)
	return newchannel
}
