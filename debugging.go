package main

import (
	"fmt"
	"html/template"
	"net/http"
	"sync/atomic"
)

type PageData struct {
	PageTitle        string
	ChannelsList     []*HubChannel
	MessageIncCount  int64
	MessageSentCount int64
	UserMessageCount int64
	UserCount        int
	ChannelCount     int
}

func debug() {
	logger(LogFields{"main", "debug", "", ""}).Debug("Channels that exist:")
	for _, c := range ChannelMap.data {
		logger(LogFields{"main", "debug", c.ID, ""}).Warn("debugging")
		logger(LogFields{"main", "debug", c.ID, ""}).Warn("debugging")
		logger(LogFields{"main", "debug", c.ID, ""}).Warn("debugging")
		logger(LogFields{"main", "debug", c.ID, ""}).Warn("debugging")
		logger(LogFields{"main", "debug", c.ID, ""}).Warn("debugging")
		logger(LogFields{"main", "debug", c.ID, ""}).Warn("debugging")
		logger(LogFields{"main", "debug", c.ID, ""}).Warn("debugging")
		logger(LogFields{"main", "debug", c.ID, ""}).Warn("debugging")

		// for _, s := range c.subscribers.data {
		// 	logger(LogFields{"main", "debug", c.ID, s.name}).Warn("debugging")
		// }
	}
	logger(LogFields{"main", "debug", "", ""}).Debug("Users that exist:")
	for _, u := range UserMap.data {
		logger(LogFields{"main", "debug", "", u.Name}).Warn("users list")
	}
}

func debugging(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("debug.html"))
	fmt.Println(ChannelMap.data)

	ch := []*HubChannel{}

	for _, v := range ChannelMap.data {
		ch = append(ch, v)
	}

	// chids := []string{}
	//  for k,v := range ChannelMap.data

	data := PageData{
		PageTitle:        "Debuggin' Page",
		ChannelsList:     ch,
		MessageIncCount:  MessageIncCount,
		MessageSentCount: MessageSentCount,
		UserMessageCount: UserMessageCount,
		UserCount:        len(UserMap.data),
		ChannelCount:     len(ChannelMap.data),
	}
	tmpl.Execute(w, data)
}

func increaseTotalIncMessageCount() {
	atomic.AddInt64(&MessageIncCount, 1)
}

func increaseTotalSentMessageCount() {
	atomic.AddInt64(&MessageSentCount, 1)
}

func increaseTotalUserMessageCount() {
	atomic.AddInt64(&UserMessageCount, 1)
}
