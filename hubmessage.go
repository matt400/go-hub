package main

import (
	"encoding/json"
	"time"
)

type ClientMessage struct {
	MessageType string `json:"type"`
	Channel     string `json:"channel"`
	Body        string `json:"body"`
}

func processUserMessage(msgtype int, message []byte, u *user) {
	increaseTotalUserMessageCount()
	// _, span := tracer.Start(u.ctx, "process-user-message")
	// defer span.End()
	switch msgtype {
	case 1:
		u.HeartBeat = time.Now()
		cm := unmarshalClientMessage(message)
		messageSwitch(cm, u)
	case 2:
		u.HeartBeat = time.Now()
		// currently unhandled binaryMessage
	case 8:
		logger(LogFields{}).Info("got a closer")
		u.socket.Close()
	case 9:
		u.HeartBeat = time.Now()
		// currently unhandled PingMessage
	case 10:
		//currently unhandled PongMessage
	}
}

func unmarshalClientMessage(msg []byte) ClientMessage {
	var clientmessage ClientMessage
	json.Unmarshal(msg, &clientmessage)
	return clientmessage
}

func messageSwitch(cm ClientMessage, u *user) {
	// _, span := tracer.Start(u.ctx, "message-switch")
	// defer span.End()

	switch cm.MessageType {
	case "hubSubscribe":
		u.joinChannel(cm.Channel)
	case "username":
		u.Name = cm.Body
	case "vanity":
		u.TellAboutSelf()
	case "debug":
		debug()
	default:
		u.HeartBeat = time.Now()
	}
}
