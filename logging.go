package main

import (
	log "github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/attribute"
	spanner "go.opentelemetry.io/otel/trace"
	"os"
)

func initLog() {
	log.SetOutput(os.Stdout)
	log.SetLevel(log.InfoLevel)
	// log.SetFormatter(&log.JSONFormatter{})
	log.Info("logger initialised")
}

type LogFields struct {
	class    string
	function string
	channel  string
	user     string
}

func logger(logfields LogFields) *log.Entry {
	l := log.WithFields(log.Fields{
		"class":    logfields.class,
		"function": logfields.function,
		"channel":  logfields.channel,
		"user":     logfields.user,
	})
	return l
}

func setUserSpanAttributes(u *user, span spanner.Span) {
	if span.IsRecording() {
		span.SetAttributes(
			attribute.String("user.id", u.ID.String()),
			attribute.String("user.name", u.Name),
		)
	}
}
