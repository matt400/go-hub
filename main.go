package main

import (
	// "github.com/go-redis/redis"
	"syscall"

	// "github.com/gomodule/redigo/redis"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"

	// "go.opentelemetry.io/otel"
	// "go.opentelemetry.io/otel/attribute"
	// "go.opentelemetry.io/otel/exporters/jaeger"
	// "go.opentelemetry.io/otel/propagation"
	// "go.opentelemetry.io/otel/sdk/resource"
	// tracesdk "go.opentelemetry.io/otel/sdk/trace"
	// semconv "go.opentelemetry.io/otel/semconv/v1.10.0"

	// "github.com/go-redis/redis/extra/redisotel/v8"

	// "go.opentelemetry.io/otel/trace"

	"net/http"
	"time"
)

const (
	service     = "gohub"
	environment = "production"
	id          = 1
)

var (
	MessageIncCount  int64
	MessageSentCount int64
	UserMessageCount int64
)

// func tracerProvider(url string) (*tracesdk.TracerProvider, error) {
// 	// Create the Jaeger exporter
// 	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url)))
// 	if err != nil {
// 		return nil, err
// 	}
// 	tp := tracesdk.NewTracerProvider(
// 		// Always be sure to batch in production.
// 		tracesdk.WithBatcher(exp),
// 		// Record information about this application in a Resource.
// 		tracesdk.WithResource(resource.NewWithAttributes(
// 			semconv.SchemaURL,
// 			semconv.ServiceNameKey.String(service),
// 			attribute.String("environment", environment),
// 			attribute.Int64("ID", id),
// 		)),
// 	)
// 	return tp, nil
// }

func maxOpenFiles() {
	var rLimit syscall.Rlimit

	err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit)
	if err != nil {
		log.Error("Error Getting Rlimit ", err)
	}

	if rLimit.Cur < rLimit.Max {
		rLimit.Cur = rLimit.Max
		err = syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rLimit)
		if err != nil {
			log.Error("Error Setting Rlimit ", err)
		}
	}
}

// func initredis() {
// 	redis.Dial("tcp", "localhost:6379")
// }

func init() {
	maxOpenFiles()
	initRedis()
	initLog()
	logger(LogFields{"main", "init", "", ""}).Info("finished init jobs")
	// tp, err := tracerProvider("http://192.168.0.206:14268/api/traces")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// otel.SetTracerProvider(tp)
	// otel.SetTextMapPropagator(propagation.TraceContext{})
}

// var tracer = otel.Tracer("channels-hub")

func socket(w http.ResponseWriter, r *http.Request) {
	var upgrader = websocket.Upgrader{
		EnableCompression: true,
		ReadBufferSize:    1024,
		WriteBufferSize:   1024,
	}
	socket, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error("could not upgrade:", err)
	}

	u := createUser(socket, r.Context())

	// defer u.remove()
	u.userProc()
}

func main() {
	addr := "0.0.0.0:80"
	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/v1/socket", socket)
	r.HandleFunc("/v1/debugging", debugging)
	go cleanStaleChannelsAndUsers()
	log.Fatal(http.ListenAndServe(addr, r))
}

func cleanStaleChannels() {
	for _, channel := range ChannelMap.data {
		if len(channel.Subscribers.Data) == 0 {
			channel.remove()
		}
	}
}

func cleanStaleUsers() {
	for _, user := range UserMap.data {
		user.checkUserHeartbeat()
	}
}

func cleanStaleChannelsAndUsers() {
	for {
		cleanStaleChannels()
		cleanStaleUsers()
		time.Sleep(60 * time.Second)
	}
}
