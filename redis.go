package main

import (
	"github.com/gomodule/redigo/redis"
)

var psc redis.PubSubConn

func initRedis() {
	logger(LogFields{"main", "initRedis", "", ""}).Info("connecting to redis")
	redisConnection, err := redis.DialURL("redis://localhost:6379")
	logger(LogFields{"main", "initRedis", "", ""}).Info("connected to redis")
	if err != nil {
		logger(LogFields{"main", "initRedis", "", ""}).Error("Error connecting to redis", err)
	}
	// defer redisConnection.Close()
	psc = redis.PubSubConn{Conn: redisConnection}
	joinRedisChannel("emptychan")
	go getRedisMessages()
}

func joinRedisChannel(channel string) {
	logger(LogFields{"main", "joinRedisChannel", channel, ""}).Info("connecting to redis: ")

	err := psc.Subscribe(channel)
	if err != nil {
		logger(LogFields{"main", "joinRedisChannel", channel, ""}).Error("Error connecting to redis channel: ", err)
	}
}

func leaveRedisChannel(channel string) {
	err := psc.Unsubscribe(channel)
	if err != nil {
		logger(LogFields{"main", "leaveRedisChannel", channel, ""}).Error("Error disconnecting from redis channel: ", err)
	}
}

func getRedisMessages() {
	for {
		switch n := psc.Receive().(type) {
		case error:
			logger(LogFields{"main", "getRedisMessages", "", ""}).Debug(n)
		case redis.Message:
			channel := ChannelMap.data[n.Channel]
			channel.handleIncMessage(n.Data)
		}
	}
}
