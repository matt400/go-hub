package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/gorilla/websocket"
)

const (
	workerCount = 30
)

type User struct {
	name    int
	conn    *websocket.Conn
	channel string
}

var wg sync.WaitGroup
var sg sync.WaitGroup

var UserList []*User

func init() {
	maxOpenFiles()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func() {
		for sig := range c {
			log.Printf("captured %v, exiting..", sig)
			wg.Add(len(UserList))
			fmt.Printf("wg: %v\n", wg)
			for _, user := range UserList {
				log.Println("closing:", user.name)
				user.conn.WriteMessage(8, []byte{})
				// user.conn.Close()
				wg.Done()
				log.Println("closed: ", user.name)
			}
			wg.Wait()
			os.Exit(1)
		}
	}()
}

func maxOpenFiles() {
	var rLimit syscall.Rlimit

	err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit)
	if err != nil {
		log.Println("Error Getting Rlimit ", err)
	}

	if rLimit.Cur < rLimit.Max {
		rLimit.Cur = rLimit.Max
		err = syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rLimit)
		if err != nil {
			handleError("Error Setting Rlimit ", err)
		}
	}
}

func writeResults(worker int, message string) {
	filename := fmt.Sprintf("results/%d_worker.log", worker)
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		handleError("could not write file ", err)
	}
	defer f.Close()
	_, err = f.WriteString(message)
	if err != nil {
		handleError("could not write message to file", err)
	}
}

func writeToSocket(message string, socket *websocket.Conn) {
	err := socket.WriteMessage(1, []byte(message))
	if err != nil {
		handleError("could not write message to socket ", err)
	}
}

func randomChannel() string {
	rand.Seed(time.Now().Unix())
	poschannels := []string{"foo", "bar"}
	randomIndex := rand.Intn(len(poschannels))
	return poschannels[randomIndex]
}

func listenToWebSocket(id int, sg sync.WaitGroup) {
	u := url.URL{Scheme: "ws", Host: "localhost", Path: "/v1/socket"}
	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		handleError("dial:", err)
	}

	// defer c.WriteMessage(websocket.CloseMessage, []byte{})
	// defer c.Close()
	user := User{
		name:    id,
		conn:    c,
		channel: randomChannel(),
	}

	UserList = append(UserList, &user)

	namerJson := strings.Replace(`{"type":"username", "body":"zibby"}`, "zibby", fmt.Sprint(id), 1)
	writeToSocket(namerJson, c)
	channelstring := strings.Replace(`{"type":"hubSubscribe","channel":"foo"}`, "foo", fmt.Sprint(user.channel), 1)
	writeToSocket(channelstring, c)

	sg.Done()

	for {
		mtype, message, err := c.ReadMessage()
		if err != nil {
			handleError("message error ", err)
		}
		if mtype == 1 {
			messageString := string(message) + "\n"
			writeResults(id, messageString)
			if id == 1 {
				log.Print("Worker:", id, " recieved ", messageString)
			}
		}
	}
}

func main() {
	sg.Add(1)
	for i := 0; i < workerCount; i++ {
		go listenToWebSocket(i, sg)
	}

	for k, v := range UserList {
		log.Println("k:", k, "v:", v)
	}

	sg.Wait()
}

func handleError(str string, err error) {
	log.Println(str, err)
}
