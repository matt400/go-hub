package main

import (
	"context"
	"fmt"
	"sync"

	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	// "go.opentelemetry.io/otel/attribute"
)

type userMap struct {
	mu   sync.Mutex
	data map[uuid.UUID]*user
}

var UserMap = userMap{
	data: make(map[uuid.UUID]*user),
}

type user struct {
	ID        uuid.UUID
	Name      string
	msgQueue  chan string
	socket    *websocket.Conn
	HeartBeat time.Time
	deleted   bool
	ctx       context.Context
}

func addToUserMap(u user) {
	// _, span := tracer.Start(u.ctx, "insert-user")
	// defer span.End()
	mu := &UserMap.mu
	mu.Lock()
	defer mu.Unlock()

	UserMap.data[u.ID] = &u

	// if span.IsRecording() {
	// 	span.SetAttributes(
	// 		attribute.String("user.id", u.id.String()),
	// 		attribute.String("user.name", u.name),
	// 	)
	// }
}

func deleteFromUserMap(u user) {
	// _, span := tracer.Start(u.ctx, "delete-user")
	// defer span.End()
	// setUserSpanAttributes(&u, span)

	// span.AddEvent("locking mutex")
	// defer span.AddEvent("unlocking mutex")
	mu := &UserMap.mu
	mu.Lock()
	defer mu.Unlock()

	delete(UserMap.data, u.ID)
}

func (u *user) remove() {
	logger(LogFields{"user", "remove", "", u.Name}).Info("Deleting user")
	// err := u.socket.Close()
	// if err != nil {
	// 	logger(LogFields{"user", "remove", "", u.name}).Error("Error closing socket:", err)
	// }

	if !IsClosed(u.msgQueue) {
		close(u.msgQueue)
	}
	u.socket.Close()
	for _, channel := range ChannelMap.data {
		channel.Subscribers.mu.Lock()
		defer channel.Subscribers.mu.Unlock()
		delete(channel.Subscribers.Data, u.ID)
	}
	u.deleted = true
	deleteFromUserMap(*u)
}

func (u *user) receiveMessages() {
	for {
		if u.deleted {
			break
		}
		msg, inc := <-u.msgQueue
		if inc {
			// _, span := tracer.Start(u.ctx, "user-message-recieved")
			// setUserSpanAttributes(u, span)
			u.socket.WriteMessage(1, []byte(msg))
			// span.End()
		}
	}
}

func (u *user) handleIncomingMessage() {
	// _, span := tracer.Start(u.ctx, "handle-incomming-message")
	// defer span.End()
	// setUserSpanAttributes(u, span)
	if u.deleted == true {
		return
	}

	msgtype, message, err := u.socket.ReadMessage()
	if err != nil {
		// span.SetAttributes(attribute.Bool("error", true))
		// span.RecordError(err)
		if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
			logger(LogFields{"user", "handleIncomingMessage", "", u.Name}).Error("Could not read message: Unexpected close error")
			return
		} else if websocket.IsCloseError(err) {
			logger(LogFields{"user", "handleIncomingMessage", "", u.Name}).Warn("Could not read message: Socket Closed Error")
			return
		} else {
			logger(LogFields{"user", "handleIncomingMessage", "", u.Name}).Warn("Could not read message")
			return
		}
	}
	processUserMessage(msgtype, message, u)
}

func (u *user) joinChannel(channelid string) {
	// _, span := tracer.Start(u.ctx, "user-join-channel")
	// defer span.End()

	logger(LogFields{"user", "joinChannel", channelid, u.Name}).Debug("joining channel")
	findOrCreateChannel(channelid, u.ctx)
	addSubscriberToChannel(u, channelid)
	// setUserSpanAttributes(u, span)
	// if span.IsRecording() {
	// 	span.SetAttributes(
	// 		attribute.String("channel.name", channelid),
	// 	)
	// }
}

func (u *user) TellAboutSelf() {
	logger(LogFields{"user", "TellAboutSelf", "", u.Name}).Debug("Printing vanity info back to user")
	u.msgQueue <- fmt.Sprintf("your name is: %s\n", u.Name)
	u.msgQueue <- fmt.Sprintf("your uuid is: %s\n", u.ID)
}

func createUser(socket *websocket.Conn, ctx context.Context) user {
	// _, span := tracer.Start(ctx, "create-user")
	// defer span.End()

	id := uuid.New()
	u := user{
		ID:        id,
		msgQueue:  make(chan string),
		socket:    socket,
		ctx:       ctx,
		HeartBeat: time.Now(),
	}
	addToUserMap(u)

	// if span.IsRecording() {
	// 	span.SetAttributes(
	// 		attribute.String("user.id", u.id.String()),
	// 		attribute.String("user.name", u.name),
	// 	)
	// }
	return u
}

func (u *user) checkUserHeartbeat() {
	if time.Now().Sub(u.HeartBeat) > 300*time.Second {
		logger(LogFields{"user", "checkUserHeartbeat", "", u.Name}).Info("deleting user because heartbeat expired")
		u.remove()
	}
}

func (u *user) userProc() {
	logger(LogFields{"user", "userProc", "", u.Name}).Debug("")
	go u.receiveMessages()
	for {
		if u.deleted {
			break
		} else {
			u.handleIncomingMessage()
		}
	}
}
